/////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  - This file is only to help the students with basic functions to interact with 
//    the DMA in the SoC
//  - You can use copy and paste any part of the code from this file in your assignment. But
//    this file itself cannot be submitted as exploit code.
//
//  - Command to run this file: ./make_run_user_with_pk.sh using_DMA
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#include "ariane_api.h"  // includes all the API functions to access the peripherals

void main(void)
{
	printf("This code shows the usage of DMA\n"); 

    //////////////////////////////////////////////////////
    // Data transfer to peripheral using DMA
    //////////////////////////////////////////////////////
    // DMA transfers data in chunks of 32 bits
    // Max length supported is 64*32-bit words
    // In this case we are transfering 4 32-bit words to the first 4 data words
    // in the SHA256 peripheral

    // write down the data u want to send as a array of 64 bit integers    
    uint64_t wdata[4]  = {0x99991111, 0x77773333,0x44441111, 0x66663333} ; // here we use 64-bit integer array instead
                                                                          // of 32-bit integer array bcz DMA fetches
                                                                          // one 64-bit word at a time for each 2-bit word

    // call DMA to transfer to peripheral
    // DMA transfer function declaration: dma_transfer_to_perif(uint32_t *sAddress, uint64_t dAddress, uint32_t length, int wait)
    // the function has 4 arguments
    // uint32_t*sAddress: a pointer to where the data to copy from is. The pointer should point to a array of 64 bit-integers 
    // uint64_t dAddress: this tells the physical address of where the data should be copied to. 
    //                    In this case, we want to copy the data to data registers of SHA. 
    //                    The base address of SHA is SHA256_BASE. The 'data' registers are starting at index 1 
    //                    in the case statement of SHA256 wrapper file (openpiton/piton/design/chip/tile/ariane/src/sha256/sha256_wrapper.sv).
    //                    so, the address will be SHA256_BASE + (1*8). We are multiplying the index 1 with 8 since 
    //                    this SoC is a 64-bit system where address are numbered 0, 8, 16, 24, ...
    // uint32_t length: the number of 32-bit words we want to transfer
    // int wait: this tells if the function should wait for transfer to complete or return while the transfer is ongoing. It
    //           is recommended to always set this to 1
    dma_transfer_to_perif(wdata, SHA256_BASE + (1*8), 0x4, 1); 


    //////////////////////////////////////////////////////
    // Data transfer from peripheral using DMA
    //////////////////////////////////////////////////////
    // DMA transfers data in chunks of 32 bits
    // Max length supported is 64*32-bit words
    // In this case we are transfering 4 32-bit words from the first 4 data words
    // in the SHA256 peripheral

    uint64_t rdata[4]; // DMA reads 32 bits at a time but returns them 
                       // as 64 bit by extending 32 bit data with 0's
                       // thats why array is 64 bit integers
    
    // call DMA to transfer from peripheral
    // DMA transfer function declaration: dma_transfer_from_perif(uint64_t sAddress, uint32_t *dAddress, uint32_t length, int wait)
    // the function has 4 arguments
    // uint64_t sAddress: this tells the physical address of from where the data should be copied. 
    //                    In this case, we want to copy the data from data registers of SHA. 
    //                    The base address of SHA is SHA256_BASE. The 'data' registers are starting at index 1 
    //                    in the case statement of SHA256 wrapper file (openpiton/piton/design/chip/tile/ariane/src/sha256/sha256_wrapper.sv).
    //                    so, the address will be SHA256_BASE + (1*8). We are multiplying the index 1 with 8 since 
    //                    wrapper verilog files ignore the first 3 bits of address, i.e., to get offset of 1, we need to do 0x8
    // uint32_t *dAddress: a pointer to where the data should be copied to. The pointer should point to a array of 64 bit-integers 
    // uint32_t length: the number of 32-bit words we want to transfer
    // int wait: this tells if the function should wait for transfer to complete or return while the transfer is ongoing. It
    //           is recommended to always set this to 1
    //
    dma_transfer_from_perif(SHA256_BASE + (1*8), rdata, 0x4, 1); 

    // print the data read from the peripheral
    printf("Data read from SHA256 = %08x %08x %08x %08x\n", rdata[0], rdata[1], rdata[2], rdata[3]); 
   

    return;

}



