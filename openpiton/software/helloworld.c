// Description: Simple hello world program 

#include <stdio.h>
#include "ariane_api.h"  // includes all the API functions to access the peripherals

int main(void) {

  printf("Starting!\n");

  printf("Welcome to ECEN426 Lab 5!\n");

  printf("Done!\n");

  return 0;
}
