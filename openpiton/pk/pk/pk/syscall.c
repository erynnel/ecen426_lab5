// See LICENSE for license details.

#include "syscall.h"
#include "pk.h"
#include "file.h"
#include "frontend.h"
#include "mmap.h"
#include "boot.h"
#include "aes0.h"
#include "aes1.h"
#include "aes2.h"
#include "sha256.h"
#include "hmac.h"
#include "common_driver_fn.h"
#include "mcall.h"
#include <string.h>
#include <errno.h>

typedef long (*syscall_t)(long, long, long, long, long, long, long);

#define CLOCK_FREQ 1000000000

void sys_exit(int code)
{
  if (current.cycle0) {
    uint64_t dt = rdtime64() - current.time0;
    uint64_t dc = rdcycle64() - current.cycle0;
    uint64_t di = rdinstret64() - current.instret0;

    printk("%lld ticks\n", dt);
    printk("%lld cycles\n", dc);
    printk("%lld instructions\n", di);
    printk("%d.%d%d CPI\n", (int)(dc/di), (int)(10ULL*dc/di % 10),
        (int)((100ULL*dc + di/2)/di % 10));
  }
  shutdown(code);
}

ssize_t sys_read(int fd, char* buf, size_t n)
{
  ssize_t r = -EBADF;
  file_t* f = file_get(fd);

  if (f)
  {
    r = file_read(f, buf, n);
    file_decref(f);
  }

  return r;
}

ssize_t sys_pread(int fd, char* buf, size_t n, off_t offset)
{
  ssize_t r = -EBADF;
  file_t* f = file_get(fd);

  if (f)
  {
    r = file_pread(f, buf, n, offset);
    file_decref(f);
  }

  return r;
}

ssize_t sys_write(int fd, const char* buf, size_t n)
{
  ssize_t r = -EBADF;
  file_t* f = file_get(fd);

  if (f)
  {
    r = file_write(f, buf, n);
    file_decref(f);
  }

  return r;
}

static int at_kfd(int dirfd)
{
  if (dirfd == AT_FDCWD)
    return AT_FDCWD;
  file_t* dir = file_get(dirfd);
  if (dir == NULL)
    return -1;
  return dir->kfd;
}

int sys_openat(int dirfd, const char* name, int flags, int mode)
{
  int kfd = at_kfd(dirfd);
  if (kfd != -1) {
    file_t* file = file_openat(kfd, name, flags, mode);
    if (IS_ERR_VALUE(file))
      return PTR_ERR(file);

    int fd = file_dup(file);
    if (fd < 0) {
      file_decref(file);
      return -ENOMEM;
    }

    return fd;
  }
  return -EBADF;
}

int sys_open(const char* name, int flags, int mode)
{
  return sys_openat(AT_FDCWD, name, flags, mode);
}

int sys_close(int fd)
{
  int ret = fd_close(fd);
  if (ret < 0)
    return -EBADF;
  return ret;
}

int sys_renameat(int old_fd, const char *old_path, int new_fd, const char *new_path) {
  int old_kfd = at_kfd(old_fd);
  int new_kfd = at_kfd(new_fd);
  if(old_kfd != -1 && new_kfd != -1) {
    size_t old_size = strlen(old_path)+1;
    size_t new_size = strlen(new_path)+1;
    return frontend_syscall(SYS_renameat, old_kfd, va2pa(old_path), old_size,
                                           new_kfd, va2pa(new_path), new_size, 0);
  }
  return -EBADF;
}

int sys_fstat(int fd, void* st)
{
  int r = -EBADF;
  file_t* f = file_get(fd);

  if (f)
  {
    r = file_stat(f, st);
    file_decref(f);
  }

  return r;
}

int sys_fcntl(int fd, int cmd, int arg)
{
  int r = -EBADF;
  file_t* f = file_get(fd);

  if (f)
  {
    r = frontend_syscall(SYS_fcntl, f->kfd, cmd, arg, 0, 0, 0, 0);
    file_decref(f);
  }

  return r;
}

int sys_ftruncate(int fd, off_t len)
{
  int r = -EBADF;
  file_t* f = file_get(fd);

  if (f)
  {
    r = file_truncate(f, len);
    file_decref(f);
  }

  return r;
}

int sys_dup(int fd)
{
  int r = -EBADF;
  file_t* f = file_get(fd);

  if (f)
  {
    r = file_dup(f);
    file_decref(f);
  }

  return r;
}

int sys_dup3(int fd, int newfd, int flags)
{
  kassert(flags == 0);
  int r = -EBADF;
  file_t* f = file_get(fd);

  if (f)
  {
    r = file_dup3(f, newfd);
    file_decref(f);
  }

  return r;
}

ssize_t sys_lseek(int fd, size_t ptr, int dir)
{
  ssize_t r = -EBADF;
  file_t* f = file_get(fd);

  if (f)
  {
    r = file_lseek(f, ptr, dir);
    file_decref(f);
  }

  return r;
}

long sys_lstat(const char* name, void* st)
{
  struct frontend_stat buf;
  size_t name_size = strlen(name)+1;
  long ret = frontend_syscall(SYS_lstat, va2pa(name), name_size, va2pa(&buf), 0, 0, 0, 0);
  copy_stat(st, &buf);
  return ret;
}

long sys_fstatat(int dirfd, const char* name, void* st, int flags)
{
  int kfd = at_kfd(dirfd);
  if (kfd != -1) {
    struct frontend_stat buf;
    size_t name_size = strlen(name)+1;
    long ret = frontend_syscall(SYS_fstatat, kfd, va2pa(name), name_size, va2pa(&buf), flags, 0, 0);
    copy_stat(st, &buf);
    return ret;
  }
  return -EBADF;
}

long sys_stat(const char* name, void* st)
{
  return sys_fstatat(AT_FDCWD, name, st, 0);
}

long sys_faccessat(int dirfd, const char *name, int mode)
{
  int kfd = at_kfd(dirfd);
  if (kfd != -1) {
    size_t name_size = strlen(name)+1;
    return frontend_syscall(SYS_faccessat, kfd, va2pa(name), name_size, mode, 0, 0, 0);
  }
  return -EBADF;
}

long sys_access(const char *name, int mode)
{
  return sys_faccessat(AT_FDCWD, name, mode);
}

long sys_linkat(int old_dirfd, const char* old_name, int new_dirfd, const char* new_name, int flags)
{
  int old_kfd = at_kfd(old_dirfd);
  int new_kfd = at_kfd(new_dirfd);
  if (old_kfd != -1 && new_kfd != -1) {
    size_t old_size = strlen(old_name)+1;
    size_t new_size = strlen(new_name)+1;
    return frontend_syscall(SYS_linkat, old_kfd, va2pa(old_name), old_size,
                                        new_kfd, va2pa(new_name), new_size,
                                        flags);
  }
  return -EBADF;
}

long sys_link(const char* old_name, const char* new_name)
{
  return sys_linkat(AT_FDCWD, old_name, AT_FDCWD, new_name, 0);
}

long sys_unlinkat(int dirfd, const char* name, int flags)
{
  int kfd = at_kfd(dirfd);
  if (kfd != -1) {
    size_t name_size = strlen(name)+1;
    return frontend_syscall(SYS_unlinkat, kfd, va2pa(name), name_size, flags, 0, 0, 0);
  }
  return -EBADF;
}

long sys_unlink(const char* name)
{
  return sys_unlinkat(AT_FDCWD, name, 0);
}

long sys_mkdirat(int dirfd, const char* name, int mode)
{
  int kfd = at_kfd(dirfd);
  if (kfd != -1) {
    size_t name_size = strlen(name)+1;
    return frontend_syscall(SYS_mkdirat, kfd, va2pa(name), name_size, mode, 0, 0, 0);
  }
  return -EBADF;
}

long sys_mkdir(const char* name, int mode)
{
  return sys_mkdirat(AT_FDCWD, name, mode);
}

long sys_getcwd(const char* buf, size_t size)
{
  populate_mapping(buf, size, PROT_WRITE);
  return frontend_syscall(SYS_getcwd, va2pa(buf), size, 0, 0, 0, 0, 0);
}

size_t sys_brk(size_t pos)
{
  return do_brk(pos);
}

int sys_uname(void* buf)
{
    if (!(__valid_user_range((uintptr_t)buf, 65*5)))
        return 0; 
  const int sz = 65;
  strcpy(buf + 0*sz, "Proxy Kernel");
  strcpy(buf + 1*sz, "");
  strcpy(buf + 2*sz, "4.15.0");
  strcpy(buf + 3*sz, "");
  strcpy(buf + 4*sz, "");
  strcpy(buf + 5*sz, "");
  return 0;
}

pid_t sys_getpid()
{
  return 0;
}

int sys_getuid()
{
  return 0;
}

uintptr_t sys_mmap(uintptr_t addr, size_t length, int prot, int flags, int fd, off_t offset)
{
#if __riscv_xlen == 32
  if (offset != (offset << 12 >> 12))
    return -ENXIO;
  offset <<= 12;
#endif
  return do_mmap(addr, length, prot, flags, fd, offset);
}

int sys_munmap(uintptr_t addr, size_t length)
{
  return do_munmap(addr, length);
}

uintptr_t sys_mremap(uintptr_t addr, size_t old_size, size_t new_size, int flags)
{
  return do_mremap(addr, old_size, new_size, flags);
}

uintptr_t sys_mprotect(uintptr_t addr, size_t length, int prot)
{
  return do_mprotect(addr, length, prot);
}

int sys_rt_sigaction(int sig, const void* act, void* oact, size_t sssz)
{
  if (oact)
    memset(oact, 0, sizeof(long) * 3);

  return 0;
}

long sys_time(long* loc)
{
    if (!(__valid_user_range((uintptr_t)loc, 8)))
        return 0; 
  uint64_t t = rdcycle64() / CLOCK_FREQ;
  if (loc)
    *loc = t;
  return t;
}

int sys_times(long* loc)
{
    if (!(__valid_user_range((uintptr_t)loc, 2*8)))
        return 0; 
  uint64_t t = rdcycle64();
  kassert(CLOCK_FREQ % 1000000 == 0);
  loc[0] = t / (CLOCK_FREQ / 1000000);
  loc[1] = 0;
  loc[2] = 0;
  loc[3] = 0;
  
  return 0;
}

int sys_gettimeofday(long* loc)
{
    if (!(__valid_user_range((uintptr_t)loc, 2*8)))
        return 0; 
  uint64_t t = rdcycle64();
  loc[0] = t / CLOCK_FREQ;
  loc[1] = (t % CLOCK_FREQ) / (CLOCK_FREQ / 1000000);
  
  return 0;
}

long sys_clock_gettime(int clk_id, long *loc)
{
    if (!(__valid_user_range((uintptr_t)loc, 2*8)))
        return 0; 
  uint64_t t = rdcycle64();
  loc[0] = t / CLOCK_FREQ;
  loc[1] = (t % CLOCK_FREQ) / (CLOCK_FREQ / 1000000000);

  return 0;
}

ssize_t sys_writev(int fd, const long* iov, int cnt)
{
  ssize_t ret = 0;
  for (int i = 0; i < cnt; i++)
  {
    ssize_t r = sys_write(fd, (void*)iov[2*i], iov[2*i+1]);
    if (r < 0)
      return r;
    ret += r;
  }
  return ret;
}

int sys_chdir(const char *path)
{
  return frontend_syscall(SYS_chdir, va2pa(path), 0, 0, 0, 0, 0, 0);
}

int sys_getdents(int fd, void* dirbuf, int count)
{
  return 0; //stub
}

static int sys_stub_success()
{
  return 0;
}

static int sys_stub_nosys()
{
  return -ENOSYS;
}

static int sys_hello_world()
{
    printk("system call hello\n"); 
    return 0;
}


static int sys_aes0_start_decry(uint32_t *ct, uint32_t *st, uint32_t key_sel)
{
    
    
    if (!(__valid_user_range((uintptr_t)ct, 16) && __valid_user_range((uintptr_t)st, 16)))
        return 0; 

    uint32_t volatile spt[4] = {ct[0], ct[1], ct[2], ct[3]} ; 

    uint32_t volatile sst[4]  = {st[0], st[1], st[2], st[3]};

    register uintptr_t a0 asm ("a0") = (uintptr_t)(spt);
    register uintptr_t a1 asm ("a1") = (uintptr_t)(sst);
    register uint32_t  a2 asm ("a2") = (uint32_t)(key_sel);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_AES0_START_ENCRYPT);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a1), "r" (a2), "r" (a7) ); 

    memset((uint32_t *)spt, 0, sizeof(spt)); 
    memset((uint32_t *)sst, 0, sizeof(sst)); 

    return 0;
}


static int sys_aes0_start_encry(uint32_t *pt, uint32_t *st, uint32_t key_sel)
{
    if (!__valid_user_range((uintptr_t)st, 16))
        return 0; 

    uint32_t volatile spt[4] = {pt[0], pt[1], pt[2], pt[3]} ; 

    uint32_t volatile sst[4]  = {st[0], st[1], st[2], st[3]};

    register uintptr_t a0 asm ("a0") = (uintptr_t)(spt);
    register uintptr_t a1 asm ("a1") = (uintptr_t)(sst);
    register uint32_t  a2 asm ("a2") = (uint32_t)(key_sel);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_AES0_START_ENCRYPT);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a1), "r" (a2), "r" (a7) ); 
    memset((uint32_t *)spt, 0, sizeof(spt)); 
    memset((uint32_t *)sst, 0, sizeof(sst)); 

    return 0;
}


static int sys_aes0_wait()
{


    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_AES0_WAIT);

    asm volatile ( "ecall"
                  : 
                  : "r" (a7) ); 

    return 0;
}


static int sys_aes0_data_out(uint32_t *t, uint32_t key_sel)
{
    if (!__valid_user_range((uintptr_t)t, 16)) 
        return 0; 

    uint32_t volatile sct[4] = {0,0,0,0};

    register uintptr_t a0 asm ("a0") = (uintptr_t)(sct);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_AES0_DATA_OUT);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a7) ); 

    t[0]  = sct[0]; 
    t[1]  = sct[1]; 
    t[2]  = sct[2]; 
    t[3]  = sct[3]; 
    memset((uint32_t *)sct, 0, sizeof(sct)); 
    return 0;
}

static int sys_aes2_start_decry(uint32_t *ct, uint32_t *st, uint32_t key_sel)
{
    if (!(__valid_user_range((uintptr_t)ct, 16) && __valid_user_range((uintptr_t)st, 16)))
        return 0; 

    uint32_t volatile spt[4] = {ct[0], ct[1], ct[2], ct[3]} ; 

    uint32_t volatile sst[4]  = {st[0], st[1], st[2], st[3]};

    register uintptr_t a0 asm ("a0") = (uintptr_t)(spt);
    register uintptr_t a1 asm ("a1") = (uintptr_t)(sst);
    register uint32_t  a2 asm ("a2") = (uint32_t)(key_sel);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_AES2_START_ENCRYPT);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a1), "r" (a2), "r" (a7) ); 

    memset((uint32_t *)spt, 0, sizeof(spt)); 
    memset((uint32_t *)sst, 0, sizeof(sst)); 

    return 0;
}


static int sys_aes2_start_encry(uint32_t *pt, uint32_t *st, uint32_t key_sel)
{
    if (!__valid_user_range((uintptr_t)st, 16))
        return 0; 

    uint32_t volatile spt[4] = {pt[0], pt[1], pt[2], pt[3]} ; 

    uint32_t volatile sst[4]  = {st[0], st[1], st[2], st[3]};

    register uintptr_t a0 asm ("a0") = (uintptr_t)(spt);
    register uintptr_t a1 asm ("a1") = (uintptr_t)(sst);
    register uint32_t  a2 asm ("a2") = (uint32_t)(key_sel);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_AES2_START_ENCRYPT);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a1), "r" (a2), "r" (a7) ); 
    memset((uint32_t *)spt, 0, sizeof(spt)); 
    memset((uint32_t *)sst, 0, sizeof(sst)); 

    return 0;
}


static int sys_aes2_wait()
{


    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_AES2_WAIT);

    asm volatile ( "ecall"
                  : 
                  : "r" (a7) ); 

    return 0;
}


static int sys_aes2_data_out(uint32_t *t, uint32_t key_sel)
{
    if (!__valid_user_range((uintptr_t)t, 16)) 
        return 0; 

    uint32_t volatile sct[4] = {0,0,0,0};

    register uintptr_t a0 asm ("a0") = (uintptr_t)(sct);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_AES2_DATA_OUT);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a7) ); 

    t[0]  = sct[0]; 
    t[1]  = sct[1]; 
    t[2]  = sct[2]; 
    t[3]  = sct[3]; 
    memset((uint32_t *)sct, 0, sizeof(sct)); 
    return 0;
}

static int sys_aes1_read_data(uint32_t *res)
{
    if (!__valid_user_range((uintptr_t)res, 12)) 
        return 0; 

    uint32_t volatile sres[4] = {0,0,0,0};

    register uintptr_t a0 asm ("a0") = (uintptr_t)(sres);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_AES1_READ_DATA);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a7) ); 

    res[0]  = sres[0]; 
    res[1]  = sres[1]; 
    res[2]  = sres[2]; 
    res[3]  = sres[3]; 
    memset((uint32_t *)sres, 0, sizeof(sres)); 
    return 0;
}
static int sys_aes1_read_config(uint32_t *conf)
{
    if (!__valid_user_range((uintptr_t)conf, 4)) 
        return 0; 

    uint32_t volatile sconf = 0;

    register uintptr_t a0 asm ("a0") = (uintptr_t)(&sconf);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_AES1_READ_CONFIG);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a7) ); 

    *conf = sconf; 
    return 0;
}
static int sys_aes1_write_data(uint32_t *bk, uint32_t key_sel)
{
    if (!__valid_user_range((uintptr_t)bk, 16)) 
        return 0; 

    uint32_t volatile sbk[4] = {bk[0], bk[1], bk[2], bk[3]} ; 

    register uintptr_t a0 asm ("a0") = (uintptr_t)(sbk);
    register uint32_t a1 asm ("a1") = (uint32_t)(key_sel);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_AES1_WRITE_DATA);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a1), "r" (a7) ); 

    memset((uint32_t *)sbk, 0, sizeof(sbk)); 
    return 0;
}
static int sys_aes1_write_config(uint32_t config)
{

    register uint32_t a0 asm ("a0") = (uint32_t)(config);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_AES1_WRITE_CONFIG);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a7) ); 

    return 0;
}


static int sys_sha256_hash(char *pString, uint32_t *hash)
{
    if (!(__valid_user_range((uintptr_t)pString, strlen(pString)+1) && __valid_user_range((uintptr_t)hash, SHA256_HASH_WORDS*4)))
        return 0; 

    char volatile spString[500];
    uint32_t volatile shash[SHA256_HASH_WORDS]; 


    memcpy((char *)spString, pString, strlen(pString)+1);


    register uintptr_t a0 asm ("a0") = (uintptr_t)(spString);
    register uintptr_t a1 asm ("a1") = (uintptr_t)(shash);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_SHA256_HASH);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a1), "r" (a7) ); 

    for (int i=0; i<SHA256_HASH_WORDS; i++)
        hash[i]  = shash[i]; 
    memset((uint32_t *)spString, 0, sizeof(spString)); 
    memset((uint32_t *)shash, 0, sizeof(shash)); 

    return 0;
}


static int sys_dma_copy(uint64_t sAddr, uint64_t dAddr, uint32_t length, uint32_t wait)
{

    uint64_t volatile ssAddr = sAddr ; 
    uint64_t volatile sdAddr = dAddr ; 
    uint32_t volatile slength = length ; 
    uint32_t volatile swait = wait ; 

    register uint64_t a0 asm ("a0") = (uint64_t)(ssAddr);
    register uint64_t a1 asm ("a1") = (uint64_t)(sdAddr);
    register uint32_t a2 asm ("a2") = (uint32_t)(slength);
    register uint32_t  a3 asm ("a3") = (uint32_t)(swait);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_DMA_COPY);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a1), "r" (a2), "r" (a3), "r" (a7) ); 

    return 0;
}

static int sys_dma_copy_to_perif(uint32_t *sAddr, uint64_t dAddr, uint32_t length, uint32_t wait)
{

    if (!__valid_user_range((uintptr_t)sAddr, length+1)) 
        return 0; 

    uint32_t volatile ssAddr[64];  
    uint64_t volatile sdAddr = dAddr ; 
    uint32_t volatile slength = length ; 
    uint32_t volatile swait = wait ; 


    if (length > 32)
        return 0; 

    for (int i=0; i<2*length; i++)
    {
        ssAddr[i] = sAddr[i]; 
    }

    register uintptr_t a0 asm ("a0") = (uintptr_t)(ssAddr);
    register uint64_t a1 asm ("a1") = (uint64_t)(sdAddr);
    register uint32_t a2 asm ("a2") = (uint32_t)(slength);
    register uint32_t  a3 asm ("a3") = (uint32_t)(wait);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_DMA_COPY);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a1), "r" (a2), "r" (a3), "r" (a7) ); 

    memset((uint32_t *)ssAddr, 0, sizeof(ssAddr)); 
    return 0;
}

static int sys_dma_copy_from_perif(uint64_t sAddr, uint32_t *dAddr, uint32_t length, uint32_t wait)
{

    if (!__valid_user_range((uintptr_t)dAddr, length+1)) 
        return 0; 

    uint64_t volatile ssAddr = sAddr;  
    uint32_t volatile sdAddr[64] ; 
    uint32_t volatile slength = length ; 
    uint32_t volatile swait = wait ; 


    if (length > 32)
        return 0; 

    for (int i=0; i<64; i++)
    {
        sdAddr[i] = 0; 
    }

    register uint64_t a0 asm ("a0") = (uint64_t)(ssAddr);
    register uintptr_t a1 asm ("a1") = (uintptr_t)(sdAddr);
    register uint32_t a2 asm ("a2") = (uint32_t)(slength);
    register uint32_t  a3 asm ("a3") = (uint32_t)(wait);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_DMA_COPY);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a1), "r" (a2), "r" (a3), "r" (a7) ); 

    for (int i=0; i<2*length; i++)
    {
        dAddr[i] = sdAddr[i]; 
    }
    memset((uint32_t *)sdAddr, 0, sizeof(sdAddr)); 
    return 0;
}


static int sys_dma_end()
{

    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_DMA_END);

    asm volatile ( "ecall"
                  : 
                  : "r" (a7) ); 

    return 0;
}






static int sys_hmac_hash(char *pString, uint32_t *hash, uint32_t use_key_hash)
{

    if (!(__valid_user_range((uintptr_t)pString, strlen(pString)+1) && __valid_user_range((uintptr_t)hash, HMAC_HASH_WORDS*4)))
        return 0; 

    char volatile spString[65];
    uint32_t volatile shash[SHA256_HASH_WORDS]; 


    memcpy((char *)spString, pString, 64);
    spString[64] = '\0'; 


    register uintptr_t a0 asm ("a0") = (uintptr_t)(spString);
    register uintptr_t a1 asm ("a1") = (uintptr_t)(shash);
    register uint32_t  a2 asm ("a2") = (uint32_t)(use_key_hash);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_HMAC_HASH);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a1), "r" (a2), "r" (a7) ); 

    for (int i=0; i<HMAC_HASH_WORDS; i++)
        hash[i]  = shash[i]; 
    memset((uint32_t *)spString, 0, sizeof(spString)); 

    return 0;
}

static int sys_cmp(uint32_t *expectedData, uint32_t *receivedData, uint32_t block_size, uint32_t *equality)
{

    if (!(__valid_user_range_u((uintptr_t)expectedData, block_size) && __valid_user_range_u((uintptr_t)receivedData, block_size)  && __valid_user_range_u((uintptr_t)equality, 4)))
        return 0; 

    uint32_t volatile spt[1024] ; 

    uint32_t volatile sst[1024]; // = receivedData; 

    uint32_t volatile sblock_size = block_size;

    uint32_t volatile sequality;

    if (block_size > 1024)
        return 0; 

    for (int i=0; i<block_size; i++)
    {
        spt[i] = expectedData[i]; 
        sst[i] = receivedData[i]; 
    }

    register uintptr_t a0 asm ("a0") = (uintptr_t)(spt);
    register uintptr_t a1 asm ("a1") = (uintptr_t)(sst);
    register uint32_t  a2 asm ("a2") = (uint32_t)(sblock_size);
    register uintptr_t a3 asm ("a3") = (uintptr_t)(&sequality);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_CMP);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a1), "r" (a2), "r" (a3), "r" (a7) ); 

    *equality = sequality;
    memset((uint32_t *)spt, 0, sizeof(spt)); 
    memset((uint32_t *)sst, 0, sizeof(sst)); 

    return 0;
}

static int sys_puts(uint32_t *buf, uint32_t buf_len)
{
    if (!(__valid_user_range((uintptr_t)buf, buf_len)))
        return 0; 

    //printk("entered puts s_sys_call \n %d: %s\n", buf_len, buf); 
    char volatile sbuf[64] ; 

    uint32_t volatile sbuf_len = buf_len; 

    for (int i=0; i<buf_len; i++)
    {
        sbuf[i] = ((char *)buf)[i]; 
    }

    register uintptr_t a0 asm ("a0") = (uintptr_t)(sbuf);
    register uint32_t a1 asm ("a1") = (uint32_t)(sbuf_len);
    register uint32_t  a7 asm ("a7") = (uint32_t)(SBI_PUTS);

    asm volatile ( "ecall"
                  : 
                  : "r" (a0), "r" (a1), "r" (a7) ); 

    return 0;
}



long do_syscall(long a0, long a1, long a2, long a3, long a4, long a5, unsigned long n)
{
  const static void* syscall_table[] = {
    [SYS_exit] = sys_exit,
    [SYS_exit_group] = sys_exit,
    [SYS_read] = sys_stub_nosys,
    [SYS_pread] = sys_stub_nosys,
    [SYS_write] = sys_stub_nosys,
    [SYS_openat] = sys_stub_nosys,
    [SYS_close] = sys_stub_nosys,
    [SYS_fstat] = sys_stub_nosys,
    [SYS_lseek] = sys_stub_nosys,
    [SYS_fstatat] = sys_stub_nosys,
    [SYS_linkat] = sys_stub_nosys,
    [SYS_unlinkat] = sys_stub_nosys,
    [SYS_mkdirat] = sys_stub_nosys,
    [SYS_renameat] = sys_stub_nosys,
    [SYS_getcwd] = sys_stub_nosys,
    [SYS_brk] = sys_brk,
    [SYS_uname] = sys_uname,
    [SYS_getpid] = sys_stub_nosys,
    [SYS_getuid] = sys_stub_nosys,
    [SYS_geteuid] = sys_stub_nosys,
    [SYS_getgid] = sys_stub_nosys,
    [SYS_getegid] = sys_stub_nosys,
    [SYS_mmap] = sys_mmap,
    [SYS_munmap] = sys_munmap,
    [SYS_mremap] = sys_mremap,
    [SYS_mprotect] = sys_mprotect,
    [SYS_prlimit64] = sys_stub_nosys,
    [SYS_rt_sigaction] = sys_stub_nosys,
    [SYS_gettimeofday] = sys_gettimeofday,
    [SYS_times] = sys_times,
    [SYS_writev] = sys_stub_nosys,
    [SYS_faccessat] = sys_stub_nosys,
    [SYS_fcntl] = sys_stub_nosys,
    [SYS_ftruncate] = sys_stub_nosys,
    [SYS_getdents] = sys_stub_nosys,
    [SYS_dup] = sys_stub_nosys,
    [SYS_dup3] = sys_stub_nosys,
    [SYS_readlinkat] = sys_stub_nosys,
    [SYS_rt_sigprocmask] = sys_stub_success,
    [SYS_ioctl] = sys_stub_nosys,
    [SYS_clock_gettime] = sys_clock_gettime,
    [SYS_getrusage] = sys_stub_nosys,
    [SYS_getrlimit] = sys_stub_nosys,
    [SYS_setrlimit] = sys_stub_nosys,
    [SYS_chdir] = sys_stub_nosys,
    [SYS_set_tid_address] = sys_stub_nosys,
    [SYS_set_robust_list] = sys_stub_nosys,
    [SYS_madvise] = sys_stub_nosys,
    [SYS_hello] = sys_hello_world,
    [SYS_AES0_START_ENCRY] = sys_aes0_start_encry,    
    [SYS_AES0_START_DECRY] = sys_aes0_start_decry,    
    [SYS_AES0_WAIT] = sys_aes0_wait,    
    [SYS_AES0_DATA_OUT] = sys_aes0_data_out,
    [SYS_AES2_START_ENCRY] = sys_aes2_start_encry,    
    [SYS_AES2_START_DECRY] = sys_aes2_start_decry,    
    [SYS_AES2_WAIT] = sys_aes2_wait,    
    [SYS_AES2_DATA_OUT] = sys_aes2_data_out,    
    [SYS_AES1_READ_DATA] = sys_aes1_read_data, 
    [SYS_AES1_WRITE_DATA] = sys_aes1_write_data,
    [SYS_AES1_READ_CONFIG] = sys_aes1_read_config, 
    [SYS_AES1_WRITE_CONFIG] = sys_aes1_write_config,
    [SYS_SHA256_HASH] = sys_sha256_hash,    
    [SYS_HMAC_HASH] = sys_hmac_hash,    
    [SYS_DMA_COPY] = sys_dma_copy,       
    [SYS_DMA_COPY1] = sys_dma_copy_to_perif,       
    [SYS_DMA_COPY2] = sys_dma_copy_from_perif,       
    [SYS_CMP] = sys_cmp,    
    [SYS_PUTS] = sys_puts,    
    [SYS_DMA_END] = sys_dma_end,    
  };

  const static void* old_syscall_table[] = {
    [-OLD_SYSCALL_THRESHOLD + SYS_open] = sys_open,
    [-OLD_SYSCALL_THRESHOLD + SYS_link] = sys_link,
    [-OLD_SYSCALL_THRESHOLD + SYS_unlink] = sys_unlink,
    [-OLD_SYSCALL_THRESHOLD + SYS_mkdir] = sys_mkdir,
    [-OLD_SYSCALL_THRESHOLD + SYS_access] = sys_access,
    [-OLD_SYSCALL_THRESHOLD + SYS_stat] = sys_stat,
    [-OLD_SYSCALL_THRESHOLD + SYS_lstat] = sys_lstat,
    [-OLD_SYSCALL_THRESHOLD + SYS_time] = sys_time,
  };

  syscall_t f = 0;

  if (n < ARRAY_SIZE(syscall_table))
    f = syscall_table[n];
  else if (n - OLD_SYSCALL_THRESHOLD < ARRAY_SIZE(old_syscall_table))
    f = old_syscall_table[n - OLD_SYSCALL_THRESHOLD];

  if (!f)
    panic("bad syscall #%ld!",n);

  return f(a0, a1, a2, a3, a4, a5, n);
}
